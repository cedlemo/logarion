type t = TomlTypes.table

let from_path path =
  match Toml.Parser.from_filename (Fpath.to_string path) with
  | `Error (str, loc) -> Error str
  | `Ok toml -> Ok toml

open TomlLenses
let (/)  a b = (key a |-- table |-- key b)
let (//) a b = (key a |-- table |-- key b |-- table)

let int toml path = get toml (path |-- int)

let float toml path = get toml (path |-- float)

let string toml path = get toml (path |-- string)

let strings toml path = get toml (path |-- array |-- strings)

let path toml path = match string toml path with Some s -> Some (Fpath.v s) | None -> None

let paths toml path = match strings toml path with
    Some ss -> Some (List.map Fpath.v ss) | None -> None
