type t = Mustache.t

let of_string = Mustache.of_string
let of_file f = File.load f |> of_string

let string s = [Html.data s]
let section ~inverted name contents = [Html.unescaped_data ("section " ^ String.concat "." name)]
let unescaped elts = [Html.unescaped_data "use escaped instead"]
let partial ?indent name _ _ = [Html.data "partials not supported"]
let comment _ = [Html.data ""]
let concat = List.concat

let escaped_index ~from ~n metas e = [Html.data "temp"]
  (* match List.hd e with *)
  (* | "topics" -> *)
  (*    let topics = *)
  (*      ListLabels.fold_left *)
  (*        ~init:(Logarion.Meta.StringSet.empty) *)
  (*        ~f:(fun a e -> Logarion.Meta.unique_topics a e ) metas *)
  (*    in *)
  (*    Logarion.Meta.StringSet.fold (fun e a -> a ^ "<li><a href=\"/topic/" ^ e ^ "\">" ^ e ^ "</a></li>") topics "" *)

let header_custom template linker archive =
  Mustache.fold ~string ~section ~escaped:(Html.Renderer.archive archive) ~unescaped ~partial ~comment ~concat template
  |> Html.header

let header_default linker archive =
  Html.(header [title [anchor (linker "/") [data archive.Logarion.Archive.Configuration.title]]])

let meta meta =
  let open Logarion.Note in
  let open Logarion.Meta in
  let abstract = meta.abstract in
  let author = meta.author.name in
  let date = Date.(pretty_date @@ last meta.date) in
  let series = stringset_csv meta.series in
  let topics = stringset_csv meta.topics in
  let keywords = stringset_csv meta.keywords in
  let uuid = Id.to_string meta.uuid in
  Html.meta ~abstract ~author ~date ~series ~topics ~keywords ~uuid

let body_custom template note =
  Mustache.fold ~string ~section ~escaped:(Html.Renderer.note note) ~unescaped ~partial ~comment ~concat template
  |> Html.note

let body_default note =
  Html.note
    [ Html.title [Html.unescaped_data note.Logarion.Note.meta.Logarion.Meta.title]; (* Don't add title if body contains one *)
      meta note.meta;
      Html.unescaped_data @@ Omd.to_html @@ Omd.of_string note.Logarion.Note.body ]

let listing url_of_meta metas =
  Html.listing url_of_meta metas

let page ~style title header body =
  Html.to_string @@ Html.page ~style title header body
                
let of_config config k = match config with
  | Error msg -> prerr_endline "Couldn't load [templates] section"; None
  | Ok c ->
     let open Confix.ConfixToml in
     path c ("templates" / k)

let converter default custom = function
  | Some p ->
     if Confix.Config.Path.path_exists p then custom @@ of_file p
     else (prerr_endline @@ "Couldn't find: " ^ Fpath.to_string p; default)
  | None -> default
          
let header_converter config = converter header_default header_custom @@ of_config config "header"
let body_converter   config = converter body_default   body_custom   @@ of_config config "body"

let page_of_listing ?(style="static/main.css") header listing archive metas =
  page ~style "Index" (header archive) (listing metas)

let page_of_note ?(style="static/main.css") header body archive note =
  page ~style note.Logarion.Note.meta.Logarion.Meta.title (header archive) (body note)

let page_of_msg ?(style="static/main.css") header archive title msg =
  page ~style title (header archive) (Html.div [Html.data msg])
