open Tyxml.Html
open Logarion

let to_string tyxml = Format.asprintf "%a" (Tyxml.Html.pp ()) tyxml

let head ~style t =
  head (title (pcdata t)) [
         link ~rel:[`Stylesheet] ~href:style ();
         link ~rel:[`Alternate]  ~href:"/feed.atom" ~a:[a_mime_type "application/atom+xml"] ();
         meta ~a:[a_charset "utf-8"] ();
       ]

let default_style = "/static/main.css"

let page ?(style=default_style) head_title header main =
  html (head ~style head_title) (body [ header; main ])

let anchor url content = a ~a:[ a_href (uri_of_string url) ] content

let div ?(style_class="") content =
  let a = if style_class <> "" then [a_class [style_class]] else [] in
  div ~a content

let unescaped_data = Unsafe.data
let data = pcdata
let title = h1
let header = header

let meta ~abstract ~author ~date ~series ~topics ~keywords ~uuid =
  details
    (summary [Unsafe.data abstract])
    [
      br ();
      a ~a:[a_rel [`Author]] [pcdata author];
      pcdata " on ";
      time ~a:[a_datetime date] [pcdata date];
      div [pcdata ("Series: " ^ series)];
      div [pcdata ("Topics: " ^ topics)];
      div [pcdata ("Keywords: " ^ keywords)];
      div [pcdata ("UUID: " ^ uuid)];
    ]

let note = article

let listing url_of_meta metas =
  let open Html in
  let item meta =
    let module Meta = Logarion.Meta in
    li [
        anchor
          (url_of_meta @@ Meta.alias meta)
          [
            time  @@ [unescaped_data Meta.Date.(pretty_date (last meta.Meta.date))];
            pcdata "\n";
            span ~a:[a_class ["title"]] [data meta.Meta.title];
          ]
      ]
  in
  ul ~a:[a_class ["timeline"]] @@ List.map item metas

module Renderer = struct
  let meta meta e =
    let e = List.hd e in
    match e with
    | "urn_name" -> [unescaped_data @@ "/note/" ^ Logarion.Meta.alias meta]
    | "date" | "date_created" | "date_edited" | "date_published" | "date_human" ->
       [time @@ [unescaped_data @@ Logarion.Meta.value_with_name meta e]]
    | tag -> [unescaped_data @@ Logarion.Meta.value_with_name meta tag]

  let note note e = match List.hd e with
    | "body" -> [unescaped_data @@ Omd.to_html @@ Omd.of_string note.Logarion.Note.body]
    | _      -> meta note.Logarion.Note.meta e

  let archive archive e = match List.hd e with
    | "title" -> [h1 [anchor ("index.html") [data archive.Logarion.Archive.Configuration.title]]]
    | tag -> prerr_endline ("unknown tag: " ^ tag); [unescaped_data ""]
end

let form default_owner default_email ymd =
  let article_form =
    let input_set title input = p [ label [ pcdata title; input ] ] in
    let either a b = if a <> "" then a else b in
    let open Note in
    let open Meta in
    let open Author in
    let auth = ymd.meta.author in
    let auth_name = either auth.name default_owner in
    let auth_addr = either (Email.to_string auth.email) default_email in
    [
      input ~a:[a_name "uuid"; a_value (Id.to_string ymd.meta.uuid); a_input_type `Hidden] ();
      input_set
        "Title"
        (input ~a:[a_name "title"; a_value ymd.meta.title; a_required ()] ());
      input_set
        "Author name"
        (input ~a:[a_name "name"; a_value auth_name] ()); 
      input_set
        "Author email"
        (input ~a:[a_name "email"; a_value auth_addr; a_input_type `Email] ());
      input_set
        "Topics"
        (input ~a:[a_name "topics"; a_value (stringset_csv ymd.meta.topics)] ());
      input_set
        "Categories"
        (input ~a:[a_name "categories"; a_value (CategorySet.to_csv ymd.meta.categories)] ());
      input_set
        "Keywords"
        (input ~a:[a_name "keywords"; a_value (stringset_csv ymd.meta.keywords)] ());
      input_set
        "Series"
        (input ~a:[a_name "series"; a_value (stringset_csv ymd.meta.series)] ());
      input_set
        "Abstract"
        (input ~a:[a_name "abstract"; a_value ymd.meta.abstract] ());
      input_set
        "Text"
        (textarea ~a:[a_name "body"] (pcdata ymd.body));
      p [ button ~a:[a_button_type `Submit] [pcdata "Submit"] ];
    ]
  in
  div
    [ form
        ~a:[a_method `Post; a_action (uri_of_string "/post.note"); a_accept_charset ["utf-8"]]
        [ fieldset ~legend:(legend [pcdata "Article"]) article_form ]
    ]
