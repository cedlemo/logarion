# Logarion

Logarion is a [free and open-source](https://joinup.ec.europa.eu/software/page/eupl) personal note taking, journaling and publication system, a blog-wiki hybrid.

[Screenshots of stock themes](https://gitlab.com/orbifx/logarion/wikis/screenshots).

## Features

Plain file system store (each note is a file).
Command line & web interfaces.

Two publishing modes:

- Static, published upon a command.
  Suitable for scenarios where installation on the server is not possible.

- Dynamic, using web server. 
  Supports interactive features like searching and more advanced Atom feed parameters.

For more features, options and guides visit the [Wiki](https://gitlab.com/orbifx/logarion/wikis).

## Community & support

- Matrix (chat): #logarion:matrix.org (Via Riot webapp: https://riot.im/app/#/room/#logarion:matrix.org)
- Reddit: https://www.reddit.com/r/logarion/
- Issues: https://gitlab.com/orbifx/logarion/issues

## Install

For development instructions see [CONTRIBUTING.md](CONTRIBUTING.md#developing-contributing).

The following instructions are the quickest way to install Logarion (in the absense of binary releases).

```
opam pin add logarion https://gitlab.com/orbifx/logarion.git
opam install logarion
```

## Configure

Before running Logarion, configure [logarion.toml](logarion.toml), which is the core configuration file.
The webserver has an additional configuration file, explained at [Web server](#web-server) section below.

## Running

Once installed you will have `logarion_cli` for command line control of the repository and `logarion_webserver` for web access.

### Command line

Run `logarion_cli --help`.

### Web server

The web server's configuration is optionally controlled by [web.toml](web.toml) in the directory the server is executed from.

Run `logarion_webserver` and open a browser to <http://localhost:3666>.
To post a new article visit <http://localhost:3666/new.note>.

Optionally install a [Sass](http://sass-lang.com/) compiler, like [sassc](http://sass-lang.com/libsass#sassc), and then run `make theme-dark` or `make theme-light`, to generate a stylesheet as `share/static/main.css`, using the respective Sass files in `share/sass/`.

## See also

- [Wiki](https://gitlab.com/orbifx/logarion/wikis)
- [CONTRIBUTING.md](CONTRIBUTING.md)
- [Licence](https://joinup.ec.europa.eu/software/page/eupl)
